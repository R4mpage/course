#include <iostream>
#include <cstring>
#include <map>

#include "Creature.h"
#include "Unicorn.h"
#include "Pegasus.h"
#include "Water.h"
#include "Earth.h"
#include "menu.h"
#include "forone.h"
#include "utility.h"
#include "List.h"


class CStringComparator {
public:
    bool operator()(const char* first, const char* second) const {
        return std::strcmp(first, second) < 0;
    }
};


using CmdMap = std::map<const char*, void (*)(const char*), CStringComparator>;


List<Creature> list;


void help(const char* line) {
    std::cout
        << "1. help -- Write help to know more" << std::endl
        << "2. load -- Loads objects from filename" << std::endl
        << "3. save -- Saves in filename" << std::endl
        << "4. print -- Print loaded" << std::endl
        << "5. remove -- Removes all of name" << std::endl
        << "6. search -- Prints first of name" << std::endl
        << "7. add -- Add new object" << std::endl;
}


void load(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Type a filename" << std::endl;
        return;
    }
    std::ifstream fin(line);
    if (!fin.good()) {
        std::cout << "Can't open this file" << line << "`" << std::endl;
        return;
    }
    fin.close();

    list.load_from(line);
}


void save(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Type a filename" << std::endl;
        return;
    }

    list.save_all(line);
}


void print(const char* line) {
    list.print_all();
}


void remove_object(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Type name of creature" << std::endl;
        return;
    }

    if (std::strcmp(line, "*") == 0) {
        list.remove_all();
    } else {
        list.remove(line);
    }
}


void search(const char* line) {
    if (!std::strlen(line)) {
        std::cout << "Type name of creature" << std::endl;
        return;
    }

    Creature* creature = list.search(line);
    if (creature == nullptr) {
        std::cout << "Can't find a creature." << std::endl;
    } else {
        std::cout << *creature;
    }
}


void add(const char* line) {
    Creature* creature;

    char classname[LENGTH] = {};
    char name[LENGTH] = {};
    char color[LENGTH] = {};
    int age;
    float hooves;
    std::cout << "Which class do you want?" 
        << "\n1.Unicorn"
        << "\n2.Pegasus"
        << "\n3.Water"
        << "\n4.Earth"
        << "\n";
    std::cin.getline(classname, LENGTH);
    std::cout << "name : ";
    std::cin.getline(name, LENGTH);
    std::cout << "color : ";
    std::cin.getline(color, LENGTH);
    std::cout << "age : ";
    std::cin >> age;
    std::cout << "quantity of hooves : ";
    std::cin >> hooves;

    if (std::strcmp(classname, "Pegasus") == 0 || std::strcmp(classname, "Unicorn") == 0) {
        char color_mane[LENGTH];
        std::cout << "color of mane : ";
        clearcin();
        std::cin.getline(color_mane, LENGTH);

        if (std::strcmp(classname, "Pegasus") == 0) {
            char color_wings[LENGTH];
            std::cout << "color of wings : ";
            std::cin.getline(color_wings, LENGTH);

            creature = new Pegasus(name, color, age, hooves, color_mane, color_wings);
        } else {
            char color_magic[LENGTH];
            std::cout << "color of magic : ";
            std::cin.getline(color_magic, LENGTH);
            
            creature = new Unicorn(name, color, age, hooves, color_mane, color_magic);
        }
    } else if (std::strcmp(classname, "Water") == 0 || std::strcmp(classname, "Earth") == 0) {
        int beak;
        std::cout << "quantity of beaks : ";
        std::cin >> beak;

        if (std::strcmp(classname, "Water") == 0) {
            int fins;
            std::cout << "quantity of fins : ";
            std::cin >> fins;

            creature = new Water(name, color, age, hooves, beak, fins);
        } else {
            int claws;
            std::cout << "quantity of claws : ";
            std::cin >> claws;

            creature = new Earth(name, color, age, hooves, beak, claws);
        }
        clearcin();
    } else {
        std::cout << "Unknows class" << std::endl;
    }
    list.append(creature);
}


void run_menu() {
    CmdMap commands;
    commands["help"] = help;
    commands["print"] = print;
    commands["load"] = load;
    commands["save"] = save;
    commands["remove"] = remove_object;
    commands["search"] = search;
    commands["add"] = add;
    std::cout << "Type `help` to know more" << std::endl;

    while (true) {
        char prompt[LENGTH] = {};
        std::cout << " - ";
        std::cin.getline(prompt, LENGTH);

        if (std::cin.fail()) {
            std::cout << "ERROR!";
            break;
        }

        if (std::strlen(prompt) == 0) {
            continue;
        }

        char prompt_copy[LENGTH];
        std::strcpy(prompt_copy, prompt);
        const char* command = std::strtok(prompt_copy, word_sep);
        const char* line = prompt_copy + std::strlen(command) + 1;
        
        if (commands.find(command) == commands.end()) {
            std::system(prompt);
            continue;
        }

        commands[command](line);
    }
}
