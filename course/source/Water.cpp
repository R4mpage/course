#include <iostream>
#include <cstring>

#include "Water.h"


Water::Water(
    const char* name,
    const char* color,
    int age,
    float hooves,
    int beak,
    int fins
) : Hyppogriff(name, color, age, hooves, beak), fins(fins) {
    classname = WATER;
}


Water::Water() : fins(2) {
    classname = WATER;
}


void Water::print(std::ostream& ostream) const {
    Hyppogriff::print(ostream);
    ostream << ", fins = " << fins << "] " << std::endl;
}


void Water::save(std::ofstream& fout) {
    Hyppogriff::save(fout);
    fout.write(reinterpret_cast<char*>(&fins), sizeof(fins));
}


void Water::load(std::ifstream& fin) {
    Hyppogriff::load(fin);
    fin.read(reinterpret_cast<char*>(&fins), sizeof(fins));
}


uint16_t Water::get_fins() const {
    return fins;
}
