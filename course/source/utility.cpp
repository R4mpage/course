#include <fstream>
#include <iostream>
#include <cstring>
#include <limits>

#include "utility.h"
#include "forone.h"
#include "Unicorn.h"
#include "Pegasus.h"
#include "Earth.h"
#include "Water.h"


Creature* read_from_file_smart(const char* filename, size_t& offset) {

    ClassName classname;
    std::ifstream fin(filename, std::ios::binary);
    fin.seekg(offset);
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.seekg(offset);

    Creature* player;
    switch (classname) {
        case PEGASUS:
            player = new Pegasus;
            break;
        case UNICORN:
            player = new Unicorn;
            break;
        case WATER:
            player = new Water;
            break;
        case EARTH:
            player = new Earth;
            break;
        default:
            return nullptr;
    }

    player->load(fin);
    
    if (fin.fail()) {
        return nullptr;
    }

    offset = fin.tellg();
    fin.close();

    return player;
}


void parse_words(const char string[], char words[][LENGTH]) {
    size_t string_length = std::strlen(string);
    char *string_copy = new char[string_length + 4]();
    std::strcpy(string_copy, string);

    char *item = std::strtok(string_copy, word_sep);
    for (size_t i = 0; item; i++) {
        std::strcpy(words[i], item);
        item = std::strtok(NULL, word_sep);
    }

    delete[] string_copy;
}

void clearcin() {
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
