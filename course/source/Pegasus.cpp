#include <iostream>
#include <cstring>

#include "Pegasus.h"


Pegasus::Pegasus(
    const char* name,
    const char* color,
    int age,
    float hooves,
    const char* color_mane,
    const char* color_wings
) : Pony(name, color, age, hooves, color_mane) {
    strcpy(this->color_wings, color_wings);
    classname = PEGASUS;
}


Pegasus::Pegasus() {
    strcpy(color_wings, "Pink");
    classname = PEGASUS;
}


void Pegasus::print(std::ostream& ostream) const {
    Pony::print(ostream);
    ostream << ", color of wings = " << color_wings << "] " << std::endl;
}

void Pegasus::save(std::ofstream& fout) {
    Pony::save(fout);
    fout.write(reinterpret_cast<char*>(color_wings), sizeof(color_wings));
}


void Pegasus::load(std::ifstream& fin) {
    Pony::load(fin);
    fin.read(reinterpret_cast<char*>(color_wings), sizeof(color_wings));
}


const char* Pegasus::get_color_wings() const {
    return color_wings;
}