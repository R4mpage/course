#pragma once
#include <map>


const size_t LENGTH = 100;

enum ClassName {
    CREATURE,
    PONY,
    HYPPOGRIFF,
    UNICORN,
    PEGASUS,
    WATER,
    EARTH
};

const char word_sep[] = " \n\t\r";
