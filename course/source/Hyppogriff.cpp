#include <iostream>
#include <cstring>

#include "Hyppogriff.h"


Hyppogriff::Hyppogriff(
    const char* name,
    const char* color,
    int age,
    float hooves,
    int beak
) : Creature(name, color, age, hooves), beak(beak) {
    classname = HYPPOGRIFF;
}


Hyppogriff::Hyppogriff() : beak(1) {
    classname = HYPPOGRIFF;
}


void Hyppogriff::print(std::ostream& ostream) const {
    Creature::print(ostream);
    ostream << ", beaks = " << beak;
}


void Hyppogriff::save(std::ofstream& fout) {
    Creature::save(fout);
    fout.write(reinterpret_cast<char*>(&beak), sizeof(beak));
}


void Hyppogriff::load(std::ifstream& fin) {
    Creature::load(fin);
    fin.read(reinterpret_cast<char*>(&beak), sizeof(beak));
}


int Hyppogriff::get_beak() const {
    return beak;
}
