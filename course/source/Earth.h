#pragma once

#include "forone.h"
#include "Hyppogriff.h"


class Earth : public Hyppogriff {
protected:
    int claws;
public:
    Earth(const char*, const char*, int, float, int, int);
    Earth();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    int get_claws() const;
};
