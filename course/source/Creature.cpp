#include <iostream>
#include <cstring>

#include "Creature.h"


Creature::Creature(
    const char* name,
    const char* color,
    int age,
    float hooves
) : age(age), hooves(hooves) {
    strcpy(this->name, name);
    strcpy(this->color, color);
    classname = CREATURE;
}


Creature::Creature() : age(0), hooves(0) {
    strcpy(name, "Creature");
    strcpy(name, "Pink");
}


const char* Creature::get_name() const {
    return name;
}


const char* Creature::get_color() const {
    return name;
}


int Creature::get_age() const {
    return age;
}


float Creature::get_hooves() const {
    return hooves;
}


ClassName Creature::get_classname() const {
    return classname;
}


void Creature::print(std::ostream& ostream) const {
    switch (classname) {
        case CREATURE:
            ostream << "Creature";
            break;
        case PONY:
            ostream << "Pony";
            break;
        case HYPPOGRIFF:
            ostream << "Hippogriff";
            break;
        case UNICORN:
            ostream << "Unicorn";
            break;
        case PEGASUS:
            ostream << "Pegasus";
            break;
        case WATER:
            ostream << "Water";
            break;
        case EARTH:
            ostream << "Earth";
            break;
        default:
            break;
    }

    ostream
        << ": "
        << name
        << " [color - " << color
        << ", age = " << age
        << ", hooves = " << hooves;
}


std::ostream& operator<<(std::ostream& ostream, const Creature& creature) {
    creature.print(ostream);
    return ostream;
}


void Creature::save(std::ofstream& fout) {
    fout.write(reinterpret_cast<char*>(&classname), sizeof(classname));
    fout.write(reinterpret_cast<char*>(name), sizeof(name));
    fout.write(reinterpret_cast<char*>(color), sizeof(color));
    fout.write(reinterpret_cast<char*>(&age), sizeof(age));
    fout.write(reinterpret_cast<char*>(&hooves), sizeof(hooves));
}


void Creature::load(std::ifstream& fin) {
    fin.read(reinterpret_cast<char*>(&classname), sizeof(classname));
    fin.read(reinterpret_cast<char*>(name), sizeof(name));
    fin.read(reinterpret_cast<char*>(color), sizeof(color));
    fin.read(reinterpret_cast<char*>(&age), sizeof(age));
    fin.read(reinterpret_cast<char*>(&hooves), sizeof(hooves));
}
