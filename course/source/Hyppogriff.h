#pragma once

#include "forone.h"
#include "Creature.h"


class Hyppogriff : public Creature {
protected:
    int beak;
public:
    Hyppogriff(const char*, const char*, int, float, int);
    Hyppogriff();

    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    int get_beak() const;
};
