#include <iostream>
#include <cstring>

#include "Earth.h"


Earth::Earth(
    const char* name,
    const char* color,
    int age,
    float hooves,
    int beak,
    int claws
) : Hyppogriff(name, color, age, hooves, beak), claws(claws) {
    classname = EARTH;
}


Earth::Earth() : claws(6) {
    classname = EARTH;
}


void Earth::print(std::ostream& ostream) const {
    Hyppogriff::print(ostream);
    ostream << ", claws = " << claws << "] " << std::endl;
}


void Earth::save(std::ofstream& fout) {
    Hyppogriff::save(fout);
    fout.write(reinterpret_cast<char*>(&claws), sizeof(claws));
}


void Earth::load(std::ifstream& fin) {
    Hyppogriff::load(fin);
    fin.read(reinterpret_cast<char*>(&claws), sizeof(claws));
}


int Earth::get_claws() const {
    return claws;
}
