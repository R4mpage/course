#pragma once

#include "Hyppogriff.h"
#include "forone.h"


class Water : public Hyppogriff {
protected:
    int fins;
public:
    Water(const char*, const char*, int, float, int, int);
    Water();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    uint16_t get_fins() const;
};
