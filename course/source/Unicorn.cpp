#include <cstring>
#include <iostream>
#include "Unicorn.h"


Unicorn::Unicorn(
    const char* name,
    const char* color,
    int age,
    float hooves,
    const char* color_mane,
    const char* color_magic
) : Pony(name, color, age, hooves, color_mane) {
    strcpy(this->color_magic, color_magic);
    classname = UNICORN;
}


Unicorn::Unicorn() {
    classname = UNICORN;
    strcpy(color_magic, "Pink");
}


void Unicorn::print(std::ostream& ostream) const {
    Pony::print(ostream);
    ostream << ", color of magic - " << color_magic << "] " << std::endl;
}


void Unicorn::save(std::ofstream& fout) {
    Pony::save(fout);
    fout.write(reinterpret_cast<char*>(color_magic), sizeof(color_magic));
}


void Unicorn::load(std::ifstream& fin) {
    Pony::load(fin);
    fin.read(reinterpret_cast<char*>(color_magic), sizeof(color_magic));
}


const char* Unicorn::get_color_magic() const {
    return color_magic;
}
