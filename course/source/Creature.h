#pragma once

#include <ostream>
#include <fstream>
#include "forone.h"


class Creature {
protected:
    ClassName classname = CREATURE;
    char name[LENGTH] = "creature";
    char color[LENGTH] = "pink";
    int age;
    float hooves;
public:
    Creature(const char*, const char*, int, float);
    Creature();

    friend std::ostream& operator<<(std::ostream&, const Creature&);

    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    const char* get_name() const;
    const char* get_color() const;
    int get_age() const;
    float get_hooves() const;
    ClassName get_classname() const;
};
