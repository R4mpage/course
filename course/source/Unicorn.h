#pragma once

#include "forone.h"
#include "Pony.h"


class Unicorn : public Pony {
protected:
    char color_magic[LENGTH];
public:
    Unicorn(const char*, const char*, int, float, const char*, const char*);
    Unicorn();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    const char* get_color_magic() const;
};
