#pragma once

#include "forone.h"
#include "Pony.h"


class Pegasus : public Pony {
protected:
    char color_wings[LENGTH];
public:
    Pegasus(const char*, const char*, int, float, const char*, const char*);
    Pegasus();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    const char* get_color_wings() const;
};
