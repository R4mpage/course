#include <iostream>
#include <cstring>

#include "Pony.h"


Pony::Pony(
    const char* name,
    const char* color,
    int age,
    float hooves,
    const char* color_mane
) : Creature(name, color, age, hooves) {
    strcpy(this->color_mane, color_mane);
    classname = PONY;
}


Pony::Pony() {
    strcpy(color_mane, "White");
    classname = PONY;
}

const char* Pony::get_color_mane() const {
    return color_mane;
}

void Pony::print(std::ostream& ostream) const {
    Creature::print(ostream);
    ostream << ", color of mane - " << color_mane;
};


void Pony::save(std::ofstream& fout) {
    Creature::save(fout);
    fout.write(reinterpret_cast<char*>(color_mane), sizeof(color_mane));
}


void Pony::load(std::ifstream& fin) {
    Creature::load(fin);
    fin.read(reinterpret_cast<char*>(color_mane), sizeof(color_mane));
}

