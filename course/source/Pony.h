#pragma once

#include "Creature.h"
#include "forone.h"


class Pony : public Creature {
protected:
    char color_mane[LENGTH] = "White";
public:
    Pony(const char*, const char*, int, float, const char*);
    Pony();

    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    const char* get_color_mane() const;

};
